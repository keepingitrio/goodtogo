var path = require('path');
var webpack = require('webpack');
var express = require('express');
var config = require('./webpack.config');
var mysql = require('mysql');

var app = express()
var compiler = webpack(config)

var connection = mysql.createConnection({
  host: "198.71.226.2",
  user: "adwipayana_hackathon",
  password: "hoBz69~4?",
  database:"adwipayana_hackathon"
});

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/getdata', (req, res) => {
  connection.query("SELECT * FROM Bathroom", function(err, rows) {
    if (!err && rows.length > 0) {
      console.log(rows)
      res.send(rows);
    } else {
      res.send(JSON.stringify(""));
    }
  })
});


app.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log('Listening at http://localhost:3000/');
});
