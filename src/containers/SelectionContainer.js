import React from 'react'
import LocationSelection from '../components/LocationSelection'

export default class SelectionContainer extends React.Component{
  constructor(props){
    super(props)

    this.state = {
      isSelected:false
    }

    this.selectBathroom = this.selectBathroom.bind(this)
    this.viewDisplay = this.viewDisplay.bind(this)
  }
  selectBathroom(id){
    let matchIdx
    let locations = this.props.location

    //clear all selected
    for (var i = 0; i < locations.length; i++) {
      locations[i].selected = false
    }

    //get selected bathroom
    for(var i = 0; i < locations.length; ++i) {
      if(locations[i].id == id) {
          matchIdx = i;
          break;
      }
    }

    //select selected bathroom
    locations[matchIdx].selected = true

    //render view
    this.setState({isSelected:true})

    //go to actual display
    const _this = this
    setTimeout(()=>{_this.viewDisplay(id)},400)
  }
  viewDisplay(id){
    this.props.onPageChange(id)
  }
  render(){
    const locationSelections = this.props.location.map((data) =>
      <LocationSelection key={data.id} id={data.id} text={data.text} status={data.status} selected={data.selected} onSelected={this.selectBathroom} />
    )
    return(
      <div className="selection content">
        <div className="selection-header">
          <p>Select your preferred bathroom location:</p>
        </div>
        <div className="selection-options">
          {locationSelections}
        </div>
      </div>
    )
  }
}
