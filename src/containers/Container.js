import React from 'react'
import Logo from '../components/Logo'
import SelectionContainer from './SelectionContainer'
import DisplayContainer from './DisplayContainer'

class Container extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      page: "selection"
    }

    this.location = [
      {id:'1flw',text:'1 Floor / West', selected:false, status:1, bathroom: [{id:'1flwa', status:false},{id:'1flwb', status:false}]},
      {id:'2flw',text:'2 Floor / West', selected:false, status:0, bathroom: [{id:'2flwa', status:false},{id:'2flwb', status:false}]},
      {id:'2fle',text:'2 Floor / East', selected:false, status:0, bathroom: [{id:'2flea', status:false},{id:'2fleb', status:false}]}
    ]

    this.viewHome = this.viewHome.bind(this)
    this.viewDisplay = this.viewDisplay.bind(this)
  }
  viewDisplay(id){
    this.setState({page:"display"})
  }
  viewHome(){
    let matchIdx
    let locations = this.location

    //clear all selected
    for (var i = 0; i < locations.length; i++) {
      locations[i].selected = false
    }

    //render home
    this.setState({page:"selection"})
  }
  render(){
    const page = this.state.page
    const isSelection = page == "selection"

    return(
      <div className="container">
        <Logo onViewHome={this.viewHome} />
        <div className="main">
          {isSelection ? <SelectionContainer location={this.location} onPageChange={this.viewDisplay} /> : <DisplayContainer />}
        </div>
      </div>
    )
  }
}

export default Container
