import React from 'react'
import CircleButton from '../components/CircleButton'

export default class DisplayContainer extends React.Component{
  constructor(props){
    super(props)
    this.data =[0,0];
    this.state = {
      loaded:false,
      test:"test"
    }
    this.interval = null
    this.getData = this.getData.bind(this)
  }
  componentDidMount(){
    this.interval = setInterval(this.getData, 1000)
  }
  componentWillUnmount(){
    clearInterval(this.interval)
  }
  getData(){
    console.log("!")
    const _this = this
    const url = 'http://frontend.gmrstage.com/hackathonapi/getavailability'
    //const url = '/getdata'
    let header = new Headers({
      'Access-Control-Allow-Origin':'*'
    });
    let sentData={
        header: header
    };
    fetch(url, {
      method:'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then((resp) => resp.json())
    .then(function(data) {
      _this.data = data.data
      _this.setState({loaded:true, test:"A"})
    })
    .catch(function(error) {
    });

  }
  render(){
    const statusList = this.data.map((data, i) =>
      <CircleButton key={i} status={data.Status} />
    )
    return(
      <div className="display content">
        {statusList}
      </div>
    )
  }
}
