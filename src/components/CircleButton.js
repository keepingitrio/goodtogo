import React from 'react'

export default class CircleButton extends React.Component{
  render(){
    const isOccupied = this.props.status == 0
    return(
      <span className={isOccupied ? "circle-button occupied" : "circle-button"}></span>
    )
  }
}
