import React from 'react'

export default class LocationSelection extends React.Component{
  constructor(props){
    super(props)

    this.selectBathroom = this.selectBathroom.bind(this)
  }
  selectBathroom(id){
    this.props.onSelected(id)
  }
  render(){
    const id = this.props.id
    const text = this.props.text
    const isActive = this.props.status == 1
    const isSelected = this.props.selected
    return(
      <div className={isActive ? "locationSelection-item" : "locationSelection-item disabled"} onClick={isActive ? this.selectBathroom.bind(this, id) : console.log('not active')}>
        <div className={isSelected ? "locationSelection-input active" : "locationSelection-input"}></div>
        <div className="locationSelection-text">{text}</div>
      </div>
    )
  }
}
