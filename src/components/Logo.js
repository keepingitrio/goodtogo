import React from 'react'
import LogoSrc from '../images/logo-web.png'

export default class Logo extends React.Component{
  constructor(props){
    super(props)
    this.renderHome = this.renderHome.bind(this)
  }
  renderHome(){
    this.props.onViewHome()
  }
  render(){
    return(
      <div className="logo" onClick={this.renderHome}>
        <img src={LogoSrc} />
      </div>
    )
  }
}
