var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    //'react-hot-loader/patch',// activate HMR for React. Comment out for prod before compiling
    //'webpack-hot-middleware/client',//Comment out for prod before compiling
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath:'dist/'
  },
  plugins: [
    //new webpack.HotModuleReplacementPlugin() //may need to get commented out for prod before compiling
  ],
  module: {
    loaders: [
      {
      test: /\.js$/,
      //loaders: ['react-hot-loader/webpack', 'babel-loader'],
      loaders: ['babel-loader'], //may need to use above for prod
      include: path.join(__dirname, 'src')
    },{
      test: /\.(scss|sass|css)$/i,
      loaders: ['style-loader', 'css-loader', 'sass-loader']
    },{
      test: /\.(png|jp(e*)g|svg)$/,
      use: [{
          loader: 'url-loader',
          options: {
              limit: 8000, // Convert images < 8kb to base64 strings
              name: 'images/[hash]-[name].[ext]'
          }
      }]
    },{
      test   : /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
      loader : 'file-loader'
    }]
  },
  resolve: {
    modules: [
      path.join(__dirname, 'node_modules'),
    ]
  }
};
